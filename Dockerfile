FROM node:8.4.0

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN npm i -g yarn
RUN yarn install

COPY . .

RUN npm run build

CMD ["npm", "start"]
