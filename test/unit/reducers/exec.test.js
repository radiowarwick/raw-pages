import execReducer from '../../../src/reducers/exec';

describe('Exec Reducer', () => {
  const members = [
    'sponsorship',
    'web',
    'sm1',
    'pc',
    'speech',
    'secretary',
    'music',
    'events',
    'news',
    'engineering',
    'production',
    'treasurer',
    'sm',
    'it',
    'arts',
    'publicity',
    'training',
    'sport',
    'visual'
  ].map(email => ({ email }));

  const reducer = execReducer(members);

  test('it initialises the state', () => {
    expect(reducer(undefined, { type: 'RANDOM' })).toEqual({
      manage: [
        { email: 'sm1' },
        { email: 'sm' },
        { email: 'treasurer' },
        { email: 'secretary' }
      ],
      offAir: [
        { email: 'sponsorship' },
        { email: 'pc' },
        { email: 'events' },
        { email: 'production' },
        { email: 'publicity' },
        { email: 'training' }
      ],
      onAir: [
        { email: 'speech' },
        { email: 'music' },
        { email: 'news' },
        { email: 'arts' },
        { email: 'sport' }
      ],
      tech: [
        { email: 'web' },
        { email: 'engineering' },
        { email: 'it' }
      ],
      visual: [
        { email: 'visual' }
      ]
    });
  });

  test('it serves up the state stored', () => {
    expect(reducer({ state: 'stale' }, { type: 'SOME_ACTION' })).toEqual({
      state: 'stale'
    });
  });
});
