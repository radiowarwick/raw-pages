import dataStore from '../../../src/reducers/data-store';

describe('Data Store Reducer', () => {
  const reducer = dataStore({ initial: 'state' });

  test('it returns the initial state', () => {
    const data = reducer(undefined, { type: 'WHATEVER' });
    expect(data).toEqual({ initial: 'state' });
  });

  test('returns whatever state is stored', () => {
    const data = reducer({ stored: 'state' }, { type: 'SOMETYPE' });
    expect(data).toEqual({ stored: 'state' });
  });
});
