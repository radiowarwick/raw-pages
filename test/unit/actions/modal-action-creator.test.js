import modalActionCreator from '../../../src/actions/modal-action-creator';
import enums from '../../../src/enums';

const dispatch = jest.fn();
const { openModal, closeModal } = modalActionCreator(dispatch);

describe('Modal Action Creator', () => {
  afterEach(() => {
    dispatch.mockReset();
  });

  describe('openModal', () => {
    test('should call dispatch with correct args', () => {
      openModal();
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith({ type: enums.ACTIONS.OPEN_MODAL });
    });
  });

  describe('closeModal', () => {
    test('should call dispatch with correct args', () => {
      closeModal();
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith({ type: enums.ACTIONS.CLOSE_MODAL });
    });
  });
});
