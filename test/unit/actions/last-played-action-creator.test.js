import actionCreator from '../../../src/actions/last-played-action-creator';
import enums from '../../../src/enums';

jest.mock('../../../config/public.js', () => ({
  plumbusAPI: 'https://test.graphql.radio.warwick.ac.uk'
}));

jest.mock('../../../src/actions/gql-queries', () => ({
  lastPlayedQuery: 'testQuery'
}));

const dispatch = jest.fn();
const axios = { post: jest.fn() };
const actions = actionCreator(dispatch);

describe('Last Played Action Creator', () => {
  afterEach(() => {
    dispatch.mockReset();
    axios.post.mockReset();
  });

  describe('getLastPlayed', () => {
    const { getLastPlayed } = actions;

    test('calls axios with correct params', async () => {
      axios.post.mockReturnValue(Promise.resolve({ data: { data: { lastPlayed: [] } } }));
      await getLastPlayed(axios);
      expect(axios.post).toHaveBeenCalledTimes(1);
      expect(axios.post).toHaveBeenCalledWith('https://test.graphql.radio.warwick.ac.uk', { query: 'testQuery' });
    });

    test('calls dispatch with correct params when successful request', async () => {
      axios.post.mockReturnValue(Promise.resolve({ data: { data: { lastPlayed: [] } } }));
      await getLastPlayed(axios);
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith({
        type: enums.ACTIONS.GET_LAST_PLAYED,
        data: []
      });
    });

    test('calls dispatch with correct params when errored request', async () => {
      axios.post.mockReturnValue(Promise.reject());
      await getLastPlayed(axios);
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith({
        type: enums.ACTIONS.GET_LAST_PLAYED_ERROR
      });
    });

    test('calls dispatch with correct params when corrupt data from request', async () => {
      axios.post.mockReturnValue(Promise.resolve({ data: {} }));
      await getLastPlayed(axios);
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith({
        type: enums.ACTIONS.GET_LAST_PLAYED_ERROR
      });
    });
  });
});
