import actionCreator from '../../../src/actions/show-action-creator';
import enums from '../../../src/enums';

jest.mock('../../../config/public.js', () => ({
  plumbusAPI: 'https://test.graphql.radio.warwick.ac.uk'
}));

jest.mock('../../../src/actions/gql-queries', () => ({
  showQuery: id => `showQuery${id}`
}));

const dispatch = jest.fn();
const axios = { post: jest.fn() };
const { getShow } = actionCreator(dispatch);

describe('Show Action Creator', () => {
  afterEach(() => {
    dispatch.mockReset();
    axios.post.mockReset();
  });

  describe('getShow', () => {
    test('calls axios with correct params', async () => {
      axios.post.mockReturnValue(Promise.resolve({ data: { data: { show: {} } } }));
      await getShow(1, axios);
      expect(axios.post).toHaveBeenCalledTimes(1);
      expect(axios.post).toHaveBeenCalledWith('https://test.graphql.radio.warwick.ac.uk', { query: 'showQuery1' });
    });

    test('calls dispatch with correct params when successful request', async () => {
      axios.post.mockReturnValue(Promise.resolve({ data: { data: { show: {} } } }));
      await getShow(1, axios);
      expect(dispatch).toHaveBeenCalledTimes(1);
      expect(dispatch).toHaveBeenCalledWith({
        type: enums.ACTIONS.GET_SHOW_DATA,
        data: {}
      });
    });

    test('doesn\'t call dispatch when errored request', async () => {
      axios.post.mockReturnValue(Promise.reject());
      await getShow(1, axios);
      expect(dispatch).not.toHaveBeenCalled();
    });

    test('doesn\'t call dispatch when corrupt data from request', async () => {
      axios.post.mockReturnValue(Promise.resolve({ data: {} }));
      await getShow(1, axios);
      expect(dispatch).not.toHaveBeenCalled();
    });
  });
});
