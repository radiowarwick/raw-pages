import moment from 'moment-timezone';

import { getMonday, getSunday, convertTime } from '../../../src/utils/date';

describe('Date Utils', () => {
  describe('getMonday', () => {
    test('should return the start of monday for a monday', () => {
      const date = getMonday(moment(1506342904000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });

    test('should return the start of monday for a tuesday', () => {
      const date = getMonday(moment(1506427200000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });

    test('should return the start of monday for a wednesday', () => {
      const date = getMonday(moment(1506513600000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });

    test('should return the start of monday for a thursday', () => {
      const date = getMonday(moment(1506600000000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });

    test('should return the start of monday for a friday', () => {
      const date = getMonday(moment(1506686400000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });

    test('should return the start of monday for a saturday', () => {
      const date = getMonday(moment(1506772800000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });

    test('should return the start of monday for a sunday', () => {
      const date = getMonday(moment(1506859200000).tz('Europe/London'));
      expect(date).toBe(1506294000);
    });
  });

  describe('getSunday', () => {
    test('should return the start of monday for a monday', () => {
      const date = getSunday(moment(1506342904000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a monday', () => {
      const date = getSunday(moment(1506342904000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a tuesday', () => {
      const date = getSunday(moment(1506427200000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a wednesday', () => {
      const date = getSunday(moment(1506513600000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a thursday', () => {
      const date = getSunday(moment(1506600000000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a friday', () => {
      const date = getSunday(moment(1506686400000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a saturday', () => {
      const date = getSunday(moment(1506772800000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });

    test('should return the start of monday for a sunday', () => {
      const date = getSunday(moment(1506859200000).tz('Europe/London'));
      expect(date).toBe(1506898800);
    });
  });

  describe('convertTime', () => {
    test('returns 12am for an hour at 0', () => {
      expect(convertTime(1506898800000)).toBe('12am');
    });

    test('returns 12pm for an hour at 12', () => {
      expect(convertTime(1506942000000)).toBe('12pm');
    });

    test('returns modulo hour for an hour greater than 12', () => {
      expect(convertTime(1506956400000)).toBe('4pm');
    });

    test('returns the hour for an hour less than 12', () => {
      expect(convertTime(1506931200000)).toBe('9am');
    });
  });
});
