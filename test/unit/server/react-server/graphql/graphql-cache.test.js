import moment from 'moment-timezone';

import { Cache } from '../../../../../server/react-server/graphql/graphql-cache';

describe('GQL Cache', () => {
  let cache;

  describe('initially', () => {
    beforeEach(() => {
      cache = new Cache(moment(1507547494000).tz('Europe/London'));
    });

    test('has an empty cache', () => {
      expect(cache.cache).toEqual({});
    });

    test('should be expired', () => {
      expect(cache.hasExpired(moment(1507544696001).tz('Europe/London'))).toBe(true);
    });

    test('should return the empty cache', () => {
      expect(cache.get()).toEqual({});
    });

    describe('set', () => {
      beforeEach(() => {
        cache.set({ foo: 'bar' }, moment(1507547494000).tz('Europe/London'));
      });

      test('should set the new cache', () => {
        expect(cache.cache).toEqual({ foo: 'bar' });
      });
    });
  });

  describe('with valid cache', () => {
    beforeEach(() => {
      cache = new Cache();
      cache.set({ foo: 'bar' }, moment(1507544400000).tz('Europe/London'));
    });

    test('should have the cache', () => {
      expect(cache.cache).toEqual({ foo: 'bar' });
    });

    test('should return the cache when get is called', () => {
      expect(cache.get()).toEqual({ foo: 'bar' });
    });

    test('should return false for expired if within 5 minutes', () => {
      expect(cache.hasExpired(moment(1507544580000).tz('Europe/London'))).toBe(false);
    });

    test('should return true for hasExpired when after 5 minutes', () => {
      expect(cache.hasExpired(moment(1507544700001).tz('Europe/London'))).toBe(true);
    });
  });

  describe('with valid cache near the hour', () => {
    beforeEach(() => {
      cache = new Cache();
      cache.set({ foo: 'bar' }, moment(1507543020000).tz('Europe/London'));
    });

    test('should have the cache', () => {
      expect(cache.cache).toEqual({ foo: 'bar' });
    });

    test('should return the cache when get is called', () => {
      expect(cache.get()).toEqual({ foo: 'bar' });
    });

    test('should return false for expired if within same hour', () => {
      expect(cache.hasExpired(moment(1507543140000).tz('Europe/London'))).toBe(false);
    });

    test('should return true for hasExpired when new hour hit', () => {
      expect(cache.hasExpired(moment(1507543200000).tz('Europe/London'))).toBe(true);
    });
  });
});
