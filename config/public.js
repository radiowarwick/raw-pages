export default (typeof window !== 'undefined' && window.rawConfig) || {
  plumbusAPI: process.env.GRAPHQL_HOST || ''
};
