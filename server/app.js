import path from 'path';

import Koa from 'koa';
import proxy from 'koa-proxy';
import mount from 'koa-mount';
import serve from 'koa-static';
import compose from 'koa-compose';
import conditional from 'koa-conditional-get';
import etag from 'koa-etag';

import renderReactServer from './react-server';

const app = new Koa();

if (process.env.NODE_ENV === 'development') {
  app.use(proxy({
    match: /^\/assets\//,
    host: 'http://localhost:8001'
  }));
}

app.use(mount('/assets/', compose([
  conditional(),
  etag({ weak: false }),
  serve(path.join(__dirname, '..', 'dist'), { gzip: true })
])));

app.use(mount('/resources/', serve(path.join(__dirname, '..', 'public'))));

app.use(renderReactServer);

export default app;
