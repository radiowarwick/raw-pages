import { getMonday, getSunday } from '../../../src/utils/date';

export default `
{
  schedule(start: ${getMonday()}, end: ${getSunday()}) {
    start
    end
    showid
    name
    description
  }
  chart {
    position
    title
    artist
    image
  }
  youtube {
    id
    title
    description
    image
  }
  exec {
    username
    name
    position
    email
    description
    pronouns
  }
  playlists {
    a_list {
      title
      artist
      image
    }
    b_list {
      title
      artist
      image
    }
    c_list {
      title
      artist
      image
    }
  }
}
`;
