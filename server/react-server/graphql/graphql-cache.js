import moment from 'moment-timezone';

export class Cache {
  constructor(now = moment().tz('Europe/London')) {
    this.cache = {};
    this.ttl = now;
    this.hourCached = -1;
  }

  hasExpired(now = moment().tz('Europe/London')) {
    return now.isAfter(this.ttl) || now.hour() !== this.hourCached;
  }

  get() {
    return this.cache;
  }

  set(value, now = moment().tz('Europe/London')) {
    this.cache = value;
    this.hourCached = now.hour();
    this.ttl = now.add(5, 'minutes');
  }
}

export default new Cache();
