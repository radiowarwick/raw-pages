import rp from 'request-promise';

import query from './graphql-queries';
import cache from './graphql-cache';

export default async () => {
  if (cache.hasExpired()) {
    const data = await rp({
      method: 'POST',
      uri: `${process.env.PLUMBUS_URL}/graphql`,
      body: { query },
      json: true
    });
    cache.set(data);
    return data;
  }
  return cache.get();
};
