import React from 'react';
import ReactDOM from 'react-dom/server';
import { createStore, combineReducers } from 'redux';
import { StaticRouter } from 'react-router';

import reducers from '../../src/reducers';
import layout from '../views/layout';
import App from '../../src/app';

import getState from './graphql/get-state';

export default async (ctx) => {
  const { data } = await getState();
  const store = createStore(combineReducers(reducers(data)));
  const context = {};
  const html = await ReactDOM.renderToString(
    <StaticRouter location={ctx.url} context={context}>
      <App store={store} />
    </StaticRouter>
  );

  if (context.url) {
    ctx.redirect(context.url);
  } else {
    ctx.status = context.status || 200;
    ctx.body = layout(html, data);
  }
};
