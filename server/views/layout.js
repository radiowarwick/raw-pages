import config from '../../config/public';

export default (html, state) => `
<!DOCTYPE html>
<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Lato|Raleway" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="apple-touch-icon" sizes="57x57" href="/resources/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/resources/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/resources/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/resources/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/resources/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/resources/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/resources/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/resources/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/resources/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/resources/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/resources/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/resources/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/resources/icons/favicon-16x16.png">
    <link rel="manifest" href="/resources/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#1a1a1a">
    <meta name="msapplication-TileImage" content="/resources/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#1a1a1a">
    <title>RAW 1251AM</title>
    ${process.env.NODE_ENV === 'development' ? '' : '<link rel="stylesheet" href="/assets/styles.css" />'}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4483020-6"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){
            dataLayer.push(arguments);
        }
        gtag("js", new Date());
        gtag("config", "UA-4483020-6");
    </script>
  </head>
  <body>
    <main id="application">${html}</main>
  </body>
  <script>
    window.initialState = ${JSON.stringify(state)};
    window.rawConfig = ${JSON.stringify(config)};
  </script>
  <script src="/assets/bundle.js"></script>
</html>
`;
