import app from './app';

const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log('[RAW Pages] Server Listening on port', port); // eslint-disable-line no-console
});
