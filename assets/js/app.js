import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { BrowserRouter as Router } from 'react-router-dom';

import reducers from '../../src/reducers';
import App from '../../src/app';

import '../scss/index.scss';

const store = createStore(combineReducers(reducers(window.initialState)));

ReactDOM.render(
  <Router>
    <App store={store} />
  </Router>,
  document.getElementById('application')
);

if (module.hot) {
  module.hot.accept('../../src/app.jsx', () => {
    ReactDOM.render(
      <Router>
        <App store={store} />
      </Router>,
      document.getElementById('application')
    );
  });
}
