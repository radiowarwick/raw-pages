const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const { config, server } = require('./webpack.config.dev');

const compiler = webpack(config());
const devServer = new WebpackDevServer(compiler, server.options);

devServer.listen(server.port, '0.0.0.0', () => {
  console.log(`[RAW Pages] Webpack development server listening on port ${server.port}. Please wait for the web server to start...`); // eslint-disable-line no-console
});
