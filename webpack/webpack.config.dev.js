const path = require('path');
const webpack = require('webpack');
const nodemon = require('nodemon');
const { babelConfig, cssLoaders, resolve } = require('./webpack.config.common');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const WEBPACK_PORT = 8001;

const startWebServer = () => {
  let server = null;

  return () => {
    if (server) {
      server.restart();
    } else {
      server = nodemon({
        exec: 'node --require babel-core/register',
        script: path.join(__dirname, '..', 'server'),
        ignore: path.join(__dirname, '..', 'src')
      }).once('quit', process.exit);
    }
  };
};

const config = () => ({
  devtool: 'inline-source-map',
  entry: [
    'react-hot-loader/patch',
    `webpack-dev-server/client?http://localhost:${WEBPACK_PORT}`,
    'webpack/hot/only-dev-server',
    path.resolve(__dirname, '../assets/js/app.js')
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: `http://localhost:${WEBPACK_PORT}/assets/`
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: Object.assign({}, babelConfig, {
              plugins: ['react-hot-loader/babel'].concat(babelConfig.plugins)
            })
          }
        ]
      },
      {
        test: /\.scss$/,
        use: [{ loader: 'style-loader' }].concat(cssLoaders)
      }
    ]
  },
  resolve,
  plugins: [
    new webpack.HotModuleReplacementPlugin(),

    new webpack.NamedModulesPlugin(),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development'),
        WEBPACK_BUILD: JSON.stringify(true)
      }
    }),

    // new BundleAnalyzerPlugin(),

    function () {
      this.plugin('done', startWebServer());
    }
  ]
});

module.exports = {
  config,
  server: {
    port: WEBPACK_PORT,
    options: {
      publicPath: '/assets/',
      contentBase: path.resolve(__dirname, '../dist'),
      hot: true,
      quiet: false,
      noInfo: false,
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      stats: {
        assets: true,
        colors: true,
        version: false,
        hash: false,
        timings: true,
        chunks: false,
        chunkModules: true
      }
    }
  }
};
