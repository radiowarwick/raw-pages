const path = require('path');
const fs = require('fs');

const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');

const { babelConfig, cssLoaders, resolve } = require('./webpack.config.common');

module.exports = [
  {
    name: 'client',
    entry: ['babel-polyfill', path.resolve(__dirname, '../assets/js/app.js')],
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, '../dist')
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: [{
            loader: 'babel-loader',
            options: babelConfig
          }]
        },
        {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: [
              cssLoaders[0],
              {
                loader: 'postcss-loader',
                options: {
                  plugins: () => [
                    autoprefixer({ browsers: 'last 4 versions' }),
                    cssnano({ safe: true, minifyGradients: false })
                  ]
                }
              },
              cssLoaders[1]
            ]
          })
        }
      ]
    },
    resolve,
    plugins: [
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production'),
          WEBPACK_BUILD: JSON.stringify(true)
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        mangle: true,
        compress: {
          warnings: false,
          screw_ie8: true,
          sequences: true,
          dead_code: true,
          drop_debugger: true,
          comparisons: true,
          conditionals: true,
          evaluate: true,
          booleans: true,
          loops: true,
          unused: true,
          hoist_funs: true,
          if_return: true,
          join_vars: true,
          cascade: true,
          drop_console: true
        },
        output: {
          comments: false
        }
      }),

      new ExtractTextPlugin('styles.css'),
      new CompressionPlugin({
        algorithm: 'gzip',
        test: /\.(js|css)$/
      })
    ]
  },
  {
    name: 'server',
    entry: path.resolve(__dirname, '../server/index.js'),
    target: 'node',
    node: {
      __dirname: false,
      __filename: false
    },
    output: {
      filename: 'server.js',
      path: path.resolve(__dirname, '../build')
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: 'babel-loader'
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx'],
      modules: [
        path.resolve(__dirname, '../src'),
        path.resolve(__dirname, '../server'),
        'node_modules'
      ]
    },
    plugins: [
      new webpack.IgnorePlugin(/\.(css|scss)$/),
      new webpack.DefinePlugin({
        'process.env.NODE_ENV': JSON.stringify('production')
      })
    ],
    externals: fs.readdirSync('node_modules')
      .filter(x => ['.bin'].indexOf(x) === -1)
      .reduce((acc, curr) => Object.assign({}, acc, { [curr]: `commonjs ${curr}` }), {})
  }
];
