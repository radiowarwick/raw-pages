const path = require('path');

module.exports = {
  babelConfig: {
    cacheDirectory: true,
    presets: [
      'react',
      ['env', {
        targets: {
          browsers: ['last 3 versions']
        }
      }]
    ],
    plugins: [
      ['transform-object-rest-spread', { useBuiltIns: true }]
    ]
  },
  cssLoaders: [
    {
      loader: 'css-loader',
      options: {
        autoprefixer: false,
        importLoaders: 1,
        minimize: false
      }
    },
    {
      loader: 'sass-loader',
      options: {
        includePaths: ['node_modules']
      }
    }
  ],
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve(__dirname, '../src'),
      path.resolve(__dirname, '../assets/js'),
      'node_modules'
    ]
  }
};
