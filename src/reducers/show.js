import enums from '../enums';

export default (state = {}, action) => {
  switch (action.type) {
  case enums.ACTIONS.GET_SHOW_DATA:
    return action.data;
  default:
    return state;
  }
};
