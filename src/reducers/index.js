import dataStore from './data-store';
import execReducer from './exec';
import showReducer from './show';
import lastPlayedReducer from './last-played';
import modalReducer from './modal';

export default (state) => {
  const {
    youtube = [],
    chart = [],
    schedule = [],
    exec = [],
    playlists = {}
  } = state;

  return {
    youtube: dataStore(youtube),
    chart: dataStore(chart),
    schedule: dataStore(schedule),
    exec: execReducer(exec),
    playlists: dataStore(playlists),
    lastPlayed: lastPlayedReducer,
    show: showReducer,
    modal: modalReducer
  };
};
