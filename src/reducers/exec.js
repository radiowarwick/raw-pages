import enums from '../enums';

const {
  EXEC_TITLES: {
    TECH_TITLES,
    MANAGEMENT_TITLES,
    ON_AIR_TITLES,
    OFF_AIR_TITLES,
    VISUAL_TITLES
  }
} = enums;

const splitRoles = (members) => {
  const value = {
    tech: [],
    offAir: [],
    onAir: [],
    manage: [],
    visual: []
  };

  members.forEach((member) => {
    const { email } = member;

    if (TECH_TITLES.indexOf(email) > -1) {
      value.tech.push(member);
    } else if (MANAGEMENT_TITLES.indexOf(email) > -1) {
      value.manage.push(member);
    } else if (ON_AIR_TITLES.indexOf(email) > -1) {
      value.onAir.push(member);
    } else if (OFF_AIR_TITLES.indexOf(email) > -1) {
      value.offAir.push(member);
    } else if (VISUAL_TITLES.indexOf(email) > -1) {
      value.visual.push(member);
    }
  });

  return { ...value, manage: value.manage.sort(({ email }) => email === 'sm' || email === 'sm1' ? -1 : 1) };
};

export default initialState => (state = splitRoles(initialState), action) => {
  switch (action.type) {
  default:
    return state;
  }
};
