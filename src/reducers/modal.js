import enums from '../enums';

export default (state = false, action) => {
  switch (action.type) {
  case enums.ACTIONS.OPEN_MODAL:
    return true;
  case enums.ACTIONS.CLOSE_MODAL:
    return false;
  default:
    return state;
  }
};
