import enums from '../enums';

export default (state = null, action) => {
  switch (action.type) {
  case enums.ACTIONS.GET_LAST_PLAYED:
    return action.data;
  case enums.ACTIONS.GET_LAST_PLAYED_ERROR:
    return { error: 'failed to load data' };
  default:
    return state;
  }
};
