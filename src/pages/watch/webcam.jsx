import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Webcam extends Component {
  constructor(props) {
    super(props);
    this.state = { lastRefresh: null };
  }

  componentDidMount() {
    if (this.props.refresh) {
      setInterval(() => {
        this.setState({ lastRefresh: Date.now() });
      }, 1000);
    }
  }

  handleClick(i) {
    return (e) => {
      e.preventDefault();
      this.props.onClick(i);
    };
  }

  render() {
    const { url, onClick, index, title } = this.props;
    const { lastRefresh } = this.state;

    const src = lastRefresh ? `${url}?t=${lastRefresh}` : url;

    const Video = (
      <div className="c-tile c-tile--video">
        <img className="c-tile__image"
          src={src}
        />
      </div>
    );

    if (onClick && typeof index !== 'undefined') {
      return (
        <div className="u-margin-bottom">
          <p className="c-text-body u-margin-bottom-tiny">{title}</p>
          <a href="#" onClick={this.handleClick(index)}>
            {Video}
          </a>
        </div>
      );
    }
    return Video;
  }
}

Webcam.propTypes = {
  refresh: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
  url: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired
};

Webcam.defaultProps = { refresh: false };

export default Webcam;
