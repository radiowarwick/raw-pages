import React, { Component } from 'react';

import webcams from '../../../data/webcams.json';
import Container from '../../components/container';

import Webcam from './webcam';

class Watch extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = { selected: 0 };
  }

  handleClick(i) {
    this.setState({ selected: i });
  }

  render() {
    const { selected } = this.state;
    const { title, url } = webcams[selected];

    return (
      <Container className="u-margin-top u-margin-bottom" title={title}>
        <div className="o-layout__item">
          <Webcam url={url} refresh />
        </div>
        <div className="o-layout__item">
          <h3 className="c-heading-delta u-margin-top">Select a webcam:</h3>
        </div>
        {webcams.map((webcam, i) => (
          <div key={webcam.url}
            className="o-layout__item u-width-1/2 u-width-1/4@medium"
          >
            <Webcam key={webcam.url}
              url={webcam.url}
              onClick={this.handleClick}
              index={i}
              title={webcam.title}
            />
          </div>
        ))}
      </Container>
    );
  }
}

export default Watch;
