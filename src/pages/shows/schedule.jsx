import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import Image from '../../components/image';
import Container from '../../components/container';
import { convertTime } from '../../utils/date';
import Tabs from '../../components/tabs';

const days = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];

class Schedule extends Component {
  constructor(props) {
    super(props);
    const currentDay = (new Date()).getDay();

    this.state = {
      startOfWeek: props.schedule[0].start,
      daySelected: (currentDay === 0) ? 7 : currentDay
    };

    this.changeDay = this.changeDay.bind(this);
  }

  getStartTime(start) {
    if (start < this.startFilter()) {
      return '12am';
    }
    return convertTime(start * 1000);
  }

  getEndTime(end) {
    if (end > this.endFilter()) {
      return '12am';
    }
    return convertTime(end * 1000);
  }

  changeDay(i) {
    this.setState({ daySelected: i + 1 });
  }

  filterByDay() {
    return this.props.schedule.filter(({ start, end }) =>
      start < this.endFilter() && end > this.startFilter()
    );
  }

  startFilter() {
    return ((this.state.daySelected - 1) * 60 * 60 * 24) + this.state.startOfWeek;
  }

  endFilter() {
    return (this.state.daySelected * 60 * 60 * 24) + this.state.startOfWeek;
  }

  render() {
    return (
      <Container className="u-margin-top" title="Schedule">
        <div className="o-layout__item u-margin-top">
          <Tabs tabs={days}
            selected={this.state.daySelected - 1}
            handleClick={this.changeDay}
          />
        </div>
        {this.filterByDay().map(({ name, showid, description, start, end }) =>
          <div key={start}
            className="o-layout__item u-margin-bottom-tiny"
          >
            <div className="c-schedule-show">
              <Link to={`/shows/${showid}`}
                className="c-faux-link"
              >
                <div className="c-schedule-show-fallback">
                  <Image className="c-schedule-show-image"
                    alt={name}
                    src={`https://media2.radio.warwick.ac.uk/static/shows/${showid}.jpg`}
                  />
                </div>
                <div className="c-schedule-show-info">
                  <p className="c-text-body-small u-margin-bottom-tiny">
                    {this.getStartTime(start)} - {this.getEndTime(end)}
                  </p>
                  <p className="c-text-lead u-margin-bottom-tiny">{name}</p>
                  {/* eslint-disable react/no-danger */}
                  <p className="c-ellipsis__four c-text-body-small c-schedule-show-description u-margin-bottom-none"
                    dangerouslySetInnerHTML={{ __html: decodeURIComponent(escape(description.replace(/<\/?[^>]+(>|$)/g, ''))) }}
                  />
                  {/* eslint-enable react/no-danger */}
                </div>
              </Link>
            </div>
          </div>
        )}
      </Container>
    );
  }
}

Schedule.propTypes = {
  schedule: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    showid: PropTypes.number,
    description: PropTypes.string,
    start: PropTypes.number,
    end: PropTypes.number
  })).isRequired
};

const mapStateToProps = ({ schedule }) => ({ schedule });

export default connect(mapStateToProps)(Schedule);
