import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import showActionCreator from '../../actions/show-action-creator';
import NotFound from '../../nav/not-found';
import Image from '../../components/image';
import { Waves } from '../../components/logo';

class Show extends Component {
  componentDidMount() {
    this.props.getShow(this.props.match.params.id);
  }

  render() {
    if (!this.props.show) {
      return <NotFound />;
    }

    const { name, id, description, presenters } = this.props.show;

    if (!id && id !== 0) {
      return <h1>Loading</h1>;
    }
    return (
      <div className="o-container u-margin-top">
        <div className="o-layout">
          <div className="o-layout__item u-width-2/3@medium">
            <Image className="c-show-info-img u-float-left u-margin-right u-margin-bottom"
              alt={name}
              src={`https://media2.radio.warwick.ac.uk/static/shows/${id}.jpg`}
            />
            <h1 className="c-heading-alpha">{name}</h1>
            {/* eslint-disable react/no-danger */}
            <p className="c-text-lead u-margin-bottom"
              dangerouslySetInnerHTML={{ __html: decodeURIComponent(escape(description.replace(/<\/?[^>]+(>|$)/g, ''))) }}
            />
          </div>
          <div className="o-layout__item u-width-1/3@medium u-margin-bottom">
            <div className="o-layout">
              {presenters.length && (
                <div className="o-layout__item">
                  <div className="c-wave-container u-margin-bottom">
                    <Waves />
                  </div>
                  <span className="c-heading-delta u-margin-left-small">
                    Presenters
                  </span>
                  {presenters.map(presenter => (
                    <div key={presenter.name} className="c-presenter u-margin-top-tiny">
                      <div className="c-presenter-image">
                        <Image alt={presenter.name}
                          src={`https://media.radio.warwick.ac.uk/members/${presenter.username}.jpg`}
                          fallback="/resources/images/user-silhouette.svg"
                        />
                      </div>
                      <p className="c-presenter-text c-text-lead u-margin-left">{presenter.name}</p>
                    </div>
                  ))}
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Show.propTypes = {
  getShow: PropTypes.func.isRequired,
  match: PropTypes.shape({ params: PropTypes.shape({ id: PropTypes.string.isRequired }).isRequired }).isRequired,
  show: PropTypes.shape({
    name: PropTypes.string,
    id: PropTypes.number,
    description: PropTypes.string,
    presenters: PropTypes.arrayOf(PropTypes.shape({ name: PropTypes.string.isRequired }))
  })
};

const mapStateToProps = ({ show }) => ({ show });
const mapDispatchToProps = dispatch => showActionCreator(dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Show);
