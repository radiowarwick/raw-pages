import React from 'react';
import { Route, Switch } from 'react-router-dom';

import NotFound from '../../nav/not-found';

import Shows from './schedule';
import Show from './show';

export default () => (
  <div>
    <Switch>
      <Route exact
        path="/shows"
        component={Shows}
      />
      <Route exact
        path="/shows/:id"
        component={Show}
      />
      <Route component={NotFound} />
    </Switch>
  </div>
);
