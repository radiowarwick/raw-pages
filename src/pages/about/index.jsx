import React from 'react';
import { Route, Switch } from 'react-router-dom';

import NotFound from '../../nav/not-found';

import Exec from './exec';

export default () => (
  <div>
    <Switch>
      <Route exact
        path="/about"
        component={Exec}
      />
      <Route component={NotFound} />
    </Switch>
  </div>
);
