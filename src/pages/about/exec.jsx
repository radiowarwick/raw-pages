import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Container from '../../components/container';

const execToTiles = exec => exec.map(({ username, position, email, name, description, pronouns }) => (
  <div key={`${email}-${username}`} className="o-layout__item u-margin-top">
    <div className="o-layout">
      <div className="o-layout__item u-width-1/4@medium">
        <img src={`https://media.radio.warwick.ac.uk/exec_images/${username}.jpg`} />
      </div>
      <div className="o-layout__item u-width-3/4@medium">
        <h3 className="c-heading-charlie u-margin-bottom-none">{name}</h3>
        <p className="c-text-lead u-margin-bottom-none">{pronouns}</p>
        <p className="c-text-lead">{position}</p>
        <p className="c-text-body">{description}</p>
      </div>
    </div>
  </div>
)
);

const Exec = ({ exec: { tech, onAir, offAir, manage, visual } }) => (
  <div className="u-margin-top">
    <Container title="Executive Committee">
      <div className="u-margin-bottom">
        {execToTiles(manage)}
      </div>
    </Container>
    <Container title="Off Air Team">
      <div className="u-margin-bottom">
        {execToTiles(offAir)}
      </div>
    </Container>
    <Container title="On Air Team">
      <div className="u-margin-bottom">
        {execToTiles(onAir)}
      </div>
    </Container>
    <Container title="Visual Team">
      <div className="u-margin-bottom">
        {execToTiles(visual)}
      </div>
    </Container>
    <Container title="Tech Team">
      <div className="u-margin-bottom">
        {execToTiles(tech)}
      </div>
    </Container>
  </div>
);

const execType = PropTypes.arrayOf(PropTypes.shape({
  username: PropTypes.string.isRequired,
  position: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  pronouns: PropTypes.string.isRequired
})).isRequired;

Exec.propTypes = {
  exec: PropTypes.shape({
    tech: execType,
    onAir: execType,
    offAir: execType,
    manage: execType,
    visual: execType
  }).isRequired
};

const mapStateToProps = ({ exec }) => ({ exec });

export default connect(mapStateToProps)(Exec);
