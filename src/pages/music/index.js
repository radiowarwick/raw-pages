/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Container from '../../components/container';
import Tile from '../../components/tile';

const tiles = list => list.map(({ title, artist, image }) => (
  <div key={title} className="u-margin-bottom u-margin-top o-layout__item u-width-1/2@small u-width-1/3@medium">
    <Tile type="square"
      alt={artist}
      image={image}
    >
      <h3 className="c-text-lead u-margin-bottom-none">{title}</h3>
      <p className="c-text-body-small u-margin-bottom-none">{artist}</p>
    </Tile>
  </div>
));

const Music = ({ a_list, b_list, c_list, chart }) => (
  <div className="u-margin-top">
    <Container title="A List"
      modifier="center"
    >
      {tiles(a_list)}
    </Container>
    <Container title="B List"
      modifier="center"
    >
      {tiles(b_list)}
    </Container>
    <Container title="C List"
      modifier="center"
    >
      {tiles(c_list)}
    </Container>
    <Container title="Chart"
      modifier="center"
    >
      {tiles(chart)}
    </Container>
  </div>
);

const listType = PropTypes.arrayOf(PropTypes.shape({
  title: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired
})).isRequired;

Music.propTypes = {
  a_list: listType,
  b_list: listType,
  c_list: listType,
  chart: listType
};

const mapStateToProps = ({ playlists, chart }) => ({ chart, ...playlists });

export default connect(mapStateToProps)(Music);
