import React from 'react';

import Container from '../../components/container';
import sponsors from '../../../data/sponsors.json';

const Sponsors = () => (
  <Container className="c-sponsors"
    title="Sponsors"
    modifier="center"
  >
    <div className="u-margin-top" />
    {sponsors.map(({ title, description, image }) =>
      <div key={title}
        className="o-layout__item u-width-1/2@medium"
      >
        <div className="o-layout o-layout--center">
          <div className="o-layout__item u-width-auto">
            <img src={image}
              alt={title}
              className="c-sponsors-image"
            />
          </div>
          <div className="o-layout__item u-width-auto u-text-center">
            <h1 className="c-text-lead u-margin-top u-margin-bottom-tiny">{title}</h1>
            <h1 className="c-text-body">{description}</h1>
          </div>
        </div>
      </div>
    )}
  </Container>
);

export default Sponsors;
