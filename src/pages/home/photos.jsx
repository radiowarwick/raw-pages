import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import Tile from '../../components/tile';
import Container from '../../components/container';

const Photos = ({ photos }) => {
  if (photos && photos.length) {
    return (
      <Container className="c-photos" title="Photos">
        <div className="u-margin-top" />
        {
          photos.slice(0, 8).map(({ image, text, url }) => (
            <div key={image}
              className="o-layout__item u-width-1/2 u-width-1/4@medium"
            >
              <a href={url}>
                <Tile alt="insta"
                  image={image}
                  type="square"
                />
                <div className="c-text-body-small u-margin-bottom u-padding-all-small c-tile-photo">
                  <div className="c-tile-photo__text">
                    <strong className="c-text-brand">raw1251am</strong> {text}
                  </div>
                </div>
              </a>
            </div>
          ))
        }
      </Container>
    );
  }
  return null;
};

Photos.propTypes = {
  photos: PropTypes.arrayOf(PropTypes.shape({
    image: PropTypes.string,
    text: PropTypes.string
  })).isRequired
};

const mapStateToProps = ({ instagram }) => ({ photos: instagram });

export default connect(mapStateToProps)(Photos);
