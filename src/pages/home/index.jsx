import React from 'react';

import Hero from '../../components/hero';

import LastPlayed from './last-played';
import Chart from './chart';
import Videos from './video';
import Sponsors from './sponsors';

export default () => (
  <div>
    <Hero>
      <LastPlayed />
    </Hero>
    <Chart />
    <Videos />
    <Sponsors />
  </div>
);
