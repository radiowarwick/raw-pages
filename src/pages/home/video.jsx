import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import YouTube from 'react-youtube';
import _ from 'lodash';

import Container from '../../components/container';
import Tile from '../../components/tile';
import Carousel from '../../components/carousel';

class Videos extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      viewed: props.videos[0].id,
      videos: props.videos && props.videos.slice(0, 9)
    };
  }

  handleClick(i) {
    this.setState({ viewed: i });
  }

  render() {
    const { viewed, videos } = this.state;
    if (videos && videos.length) {
      const selected = _.find(videos, ({ id }) => id === viewed);
      return (
        <Container className="c-videos u-margin-bottom"
          title="Videos"
        >
          <div className="o-layout__item u-margin-top u-margin-bottom">
            <div className="c-tile c-tile--video">
              <YouTube videoId={selected.id}
                className="c-youtube-video"
                onStateChange={(e) => {
                  if (e.data === 5) {
                    e.target.playVideo();
                  }
                }}
              />
            </div>
          </div>
          <Carousel>
            {
              videos
                .filter(({ id }) => id !== viewed)
                .map(({ title, image, id }) => (
                  <div key={id}
                    className="u-width-1/2 u-width-1/4@medium"
                  >
                    <div tabIndex="0"
                      role="button"
                      className="o-carousel__item"
                      onClick={() => this.handleClick(id)}
                    >
                      <Tile alt={title}
                        image={image}
                        type="video"
                      />
                      <h3 className="c-ellipsis__two u-text-center c-text-body-small u-margin-bottom-none">{title}</h3>
                    </div>
                  </div>
                ))
            }
          </Carousel>
        </Container>
      );
    }
    return null;
  }
}

Videos.propTypes = {
  videos: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired
  })).isRequired
};

const mapStateToProps = ({ youtube }) => ({ videos: youtube });

export default connect(mapStateToProps)(Videos);
