import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Tile from '../../components/tile';
import Container from '../../components/container';

const Chart = ({ tracks }) => (
  <Container className="c-chart u-margin-bottom u-margin-top" title="Chart">
    <div className="u-margin-top" />
    {
      tracks.map(({ position, title, image }, i) => {
        const className = classnames('o-layout__item u-width-1/2 c-chart__item', {
          'u-width-1/2@medium u-float-left': i === 0,
          'u-width-1/4@medium c-chart-spacer': i > 0 && i < 5,
          'u-width-1/5@medium': i > 4
        });
        return (
          <div key={position} className={className}>
            <Tile type="square"
              alt={title}
              image={image}
              position={position}
            >
              <h3 className="c-text-lead u-margin-bottom-none c-tile-chart__title">{title}</h3>
            </Tile>
          </div>
        );
      })
    }
  </Container>
);

Chart.propTypes = {
  tracks: PropTypes.arrayOf(PropTypes.shape({
    postion: PropTypes.number,
    track: PropTypes.shape({
      title: PropTypes.string,
      artist: PropTypes.string
    })
  })).isRequired
};

const mapStateToProps = ({ chart }) => ({ tracks: chart });

export default connect(mapStateToProps)(Chart);
