import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import actionCreator from '../../actions/last-played-action-creator';

const Track = ({ image, title, artist, type }) => (
  <div className={`c-last-played-track c-last-played-track--${type}`}>
    <div className="c-last-played-track-image">
      <img alt={`${type} track`}
        src={image || '/resources/images/track.png'}
      />
    </div>
    <div className="c-track-info">
      { type === 'main' &&
        <div className="c-text-lead">
          <span className="c-show-on-air">
            Last Played
          </span>
        </div>
      }
      <div className="c-last-played-text c-text-body u-text-bold">{title}</div>
      <div className="c-last-played-text c-text-body-small">{artist}</div>
    </div>
  </div>
);

Track.propTypes = {
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  artist: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
};

class LastPlayed extends Component {
  componentDidMount() {
    this.lastCheck = Date.now();
    this.props.getLastPlayed();
    this.refresfer = setInterval(this.props.getLastPlayed, 1000 * 60);
  }

  componentWillUnmount() {
    clearInterval(this.refresfer);
  }

  renderContent() {
    const { tracks } = this.props;
    if (!tracks) {
      return (
        <div className="c-last-played-spinner">
          <div className="c-spinner">Loading</div>
        </div>
      );
    }
    if (tracks.error) {
      return <div>Error</div>;
    }

    return tracks.map((track, i) => <Track key={i} {...track} type={i === 0 ? 'main' : 'played'} />); // eslint-disable-line react/no-array-index-key
  }

  render() {
    return (
      <div className="c-last-played">
        {this.renderContent()}
      </div>
    );
  }
}

LastPlayed.propTypes = {
  getLastPlayed: PropTypes.func.isRequired,
  tracks: PropTypes.array
};

const mapStateToProps = ({ lastPlayed }) => ({ tracks: lastPlayed });

const mapDispatchToProps = dispatch => actionCreator(dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(LastPlayed);
