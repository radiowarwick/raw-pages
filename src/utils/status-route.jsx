import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

const StatusRoute = ({ code, children }) => (
  <Route render={({ staticContext }) => {
    if (staticContext) {
        staticContext.status = code; // eslint-disable-line
    }
    return children;
  }}
  />
);

StatusRoute.propTypes = {
  code: PropTypes.number.isRequired,
  children: PropTypes.node.isRequired
};

export default StatusRoute;
