export const viewWidth = () => window.innerWidth
  || document.documentElement.clientWidth
  || document.body.clientWidth;
