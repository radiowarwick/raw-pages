import axios from 'axios';

import config from '../../config/public';

const escapeString = str => str.replace(/[<>`${}|&"'\\/]/g, '');

export default (name, message) => axios.post(config.plumbusAPI, {
  query: `
  mutation {
    message(name: "${escapeString(name)}", message: "${escapeString(message)}")
  }`
})
  .then(({ data }) => data && data.data && data.data.message)
  .catch(() => false);
