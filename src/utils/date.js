import moment from 'moment-timezone';

export const getMonday = (now = moment().tz('Europe/London')) => {
  const d = now.day();
  if (d > 0) {
    now.day('Monday');
  } else {
    now.day(-6);
  }
  now.hour(0);
  now.minute(0);
  now.second(0);
  now.millisecond(0);
  return now.unix();
};

export const getSunday = (now = moment().tz('Europe/London')) => {
  const day = now.day();

  if (day > 0) {
    now.day(7);
  }
  now.hour(24);
  now.minute(0);
  now.second(0);
  now.millisecond(0);

  return now.unix();
};

export const convertTime = (time) => {
  const h = moment(time)
    .tz('Europe/London')
    .hour();

  if (h === 0) {
    return '12am';
  }

  if (h === 12) {
    return '12pm';
  }

  if (h > 12) {
    return `${h % 12}pm`;
  }

  return `${h}am`;
};
