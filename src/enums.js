export default {
  ACTIONS: {
    GET_SHOW_DATA: 'GET_SHOW_DATA',
    GET_LAST_PLAYED: 'GET_LAST_PLAYED',
    GET_LAST_PLAYED_ERROR: 'GET_LAST_PLAYED_ERROR',
    OPEN_MODAL: 'OPEN_MODAL',
    CLOSE_MODAL: 'CLOSE_MODAL'
  },
  EXEC_TITLES: {
    TECH_TITLES: [
      'web',
      'engineering',
      'it'
    ],
    ON_AIR_TITLES: [
      'news',
      'music',
      'arts',
      'speech',
      'sport'
    ],
    OFF_AIR_TITLES: [
      'events',
      'production',
      'publicity',
      'social',
      'sponsorship',
      'training',
      'pc',
      'marketing'
    ],
    MANAGEMENT_TITLES: [
      'treasurer',
      'secretary',
      'sm',
      'sm1'
    ],
    VISUAL_TITLES: [
      'visual',
      'postproduction',
      'factual',
      'entertainment'
    ]
  }
};
