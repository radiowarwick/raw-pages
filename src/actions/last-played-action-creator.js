import axios from 'axios';

import config from '../../config/public';
import enums from '../enums';

import { lastPlayedQuery } from './gql-queries';

const getLastPlayed = dispatch => (_axios = axios) => _axios.post(config.plumbusAPI, { query: lastPlayedQuery })
  .then(({ data: { data: { lastPlayed } } }) => {
    dispatch({ type: enums.ACTIONS.GET_LAST_PLAYED, data: lastPlayed });
  })
  .catch(() => {
    dispatch({ type: enums.ACTIONS.GET_LAST_PLAYED_ERROR });
  });

export default dispatch => ({
  getLastPlayed: getLastPlayed(dispatch)
});
