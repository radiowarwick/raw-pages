import enums from '../enums';

export default dispatch => ({
  openModal() {
    dispatch({ type: enums.ACTIONS.OPEN_MODAL });
  },

  closeModal() {
    dispatch({ type: enums.ACTIONS.CLOSE_MODAL });
  }
});
