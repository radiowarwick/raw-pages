export const showQuery = id => `
{
  show(id: ${id}) {
    id
    name
    description
    presenters {
      id
      username
      name
    }
  }
}
`;

export const lastPlayedQuery = `
{
  lastPlayed {
    title
    artist
    image
  }
}
`;
