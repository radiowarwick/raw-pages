import axios from 'axios';

import config from '../../config/public';
import enums from '../enums';

import { showQuery } from './gql-queries';

export default dispatch => ({
  getShow(id, _axios = axios) {
    return _axios.post(config.plumbusAPI, { query: showQuery(id) })
      .then(({ data: { data: { show } } }) => {
        dispatch({ type: enums.ACTIONS.GET_SHOW_DATA, data: show });
      })
      .catch(noop => noop);
  }
});
