import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import classnames from 'classnames';

import Message from './components/message';
import Nav from './nav';
import actionCreator from './actions/modal-action-creator';

const Page = ({ children, modalOpen, actions }) => (
  <div className={classnames({ 'u-body-lock': modalOpen })}>
    <Message isOpen={modalOpen}
      closeModal={actions.closeModal}
    />
    <Nav openModal={actions.openModal}>
      {children}
    </Nav>
  </div>
);

Page.propTypes = {
  children: PropTypes.node.isRequired,
  modalOpen: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired
};

const mapStateToProps = ({ modal }) => ({ modalOpen: modal });

const mapDispatchToProps = dispatch => ({ actions: actionCreator(dispatch) });

export default connect(mapStateToProps, mapDispatchToProps)(Page);
