import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Image extends Component {
  constructor(props) {
    super(props);

    this.state = { errored: false };
    this.onError = this.onError.bind(this);
  }

  componentDidMount() {
    this.forceUpdate();
  }

  onError(e) {
    e.preventDefault();
    this.setState({ errored: true });
  }

  render() {
    const { src, alt, className, fallback } = this.props;

    if (this.state.errored && !fallback) {
      return null;
    }

    const actualSRC = this.state.errored ? fallback : src;

    return (
      <img src={actualSRC}
        alt={alt}
        className={className}
        onError={this.onError}
      />
    );
  }
}

Image.defaultProps = { className: '' };

Image.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  className: PropTypes.string,
  fallback: PropTypes.string
};

export default Image;
