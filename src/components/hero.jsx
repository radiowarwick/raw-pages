import React from 'react';
import PropTypes from 'prop-types';

const Hero = ({ children }) => (
  <div className="o-layout">
    <div className="o-layout__item c-hero">
      <video src="https://media.radio.warwick.ac.uk/video/timelapse.mp4"
        loop
        muted
        autoPlay
      />
      <div className="o-container">
        <div className="o-layout o-layout--right">
          <div className="o-layout__item c-last-played-container">
            { children }
          </div>
        </div>
      </div>
    </div>
  </div>
);

Hero.propTypes = { children: PropTypes.node.isRequired };

export default Hero;
