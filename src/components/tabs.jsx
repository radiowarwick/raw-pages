import React from 'react';
import pt from 'prop-types';
import classnames from 'classnames';

const Tabs = ({ tabs, selected, handleClick }) => (
  <ul className="c-tabs">
    {
      tabs.map((tab, i) => {
        const className = classnames('c-tabs__item c-text-body', { 'is-active': i === selected });
        return (
          <li key={tab}
            className="c-tabs__item-container"
            style={{ width: `${100 / tabs.length}%` }}
          >
            <div tabIndex="0"
              role="link"
              className={className}
              onClick={() => handleClick(i)}
            >
              {tab}
            </div>
          </li>
        );
      })
    }
  </ul>
);

Tabs.propTypes = {
  tabs: pt.arrayOf(pt.string).isRequired,
  selected: pt.number.isRequired,
  handleClick: pt.func.isRequired
};

export default Tabs;
