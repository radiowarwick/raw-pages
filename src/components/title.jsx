import React from 'react';
import PropTypes from 'prop-types';

import { Waves } from './logo';

const Title = ({ title }) => (
  <div className="o-container c-title">
    <div className="o-layout">
      <div className="o-layout__item u-padding-top-small u-padding-bottom-small">
        <div className="c-title-waves"><Waves /></div>
        <h1 className="c-title-heading c-heading-charlie u-margin-bottom-none">{ title }</h1>
      </div>
    </div>
  </div>
);

Title.propTypes = { title: PropTypes.string.isRequired };

export default Title;
