import React from 'react';
import PropTypes from 'prop-types';

import Title from './title';

const Container = ({ className, title, children, modifier }) => {
  let containerClass = 'o-layout u-clearfix';
  containerClass = modifier ? `${containerClass} o-layout--${modifier}` : containerClass;
  return (
    <div className={className}>
      <Title title={title} />
      <div className="o-container">
        <div className={containerClass}>
          { children }
        </div>
      </div>
    </div>
  );
};

Container.defaultProps = {
  modifier: '',
  className: ''
};

Container.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
  modifier: PropTypes.string
};

export default Container;
