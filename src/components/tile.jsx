import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const getType = (type) => {
  switch (type) {
  case 'square':
    return 'c-tile--square';
  case 'video':
    return 'c-tile--video';
  default:
    return null;
  }
};

const Tile = ({ image, alt, children, type, position }) => (
  <div className="c-tile__container">
    <div className={classnames('c-tile', 'c-tile--zoom', getType(type))}>
      <img className="c-tile__image"
        alt={alt}
        src={image || '/resources/images/track.png'}
      />
      {position && <div className="c-tile__position">{position}</div>}
      <div className="c-tile__content u-padding-all-small">
        { children }
      </div>
    </div>
  </div>
);

Tile.defaultProps = {
  children: null,
  position: 0
};

Tile.propTypes = {
  image: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  children: PropTypes.node,
  type: PropTypes.oneOf(['square', 'video', null]),
  position: PropTypes.number
};

Tile.defaultProps = { type: null };

export default Tile;
