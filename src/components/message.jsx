import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import messageService from '../utils/message';

class Message extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      name: '',
      message: '',
      submitted: false,
      isLoading: false,
      error: false
    };
  }

  handleChange(e) {
    const { value, name } = e.target;

    if (value) {
      this.setState({ [name]: value, submitted: false });
    }
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({ submitted: true, error: false });
    const name = e.target.name.value;
    const message = e.target.message.value;
    if (name && message) {
      this.setState({ isLoading: true });
      //  TODO: Write failure handling
      messageService(name, message)
        .then((success) => {
          if (success) {
            this.props.closeModal();
            setTimeout(() => {
              this.setState({ isLoading: false });
            }, 500);
          } else {
            this.setState({ error: true, isLoading: false });
          }
        })
        .catch(() => {
          this.setState({ error: true, isLoading: false });
        });
    }
  }

  render() {
    const { isOpen, closeModal } = this.props;
    const { submitted, name, message, error } = this.state;
    return (
      <div className={classnames('c-modal', { 'is-open': isOpen })}>
        <div className="c-modal__content">
          <div className="c-modal__card">
            {this.state.isLoading ? <div className="c-spinner" /> : (
              <div>
                <h1 className="c-heading-alpha u-margin-bottom-none">Message the Studio</h1>
                {error && (
                  <p className="u-error u-margin-all-none">Something went wrong please try again later</p>
                )}
                <form onSubmit={this.handleSubmit}>
                  <div className="o-layout">
                    <div className="o-layout__item u-margin-bottom-small">
                      <label className="c-text-body" htmlFor="name">Name</label>
                      <input className="c-input"
                        type="text"
                        name="name"
                        value={name}
                        onChange={this.handleChange}
                      />
                      {(submitted && !name && (
                        <p className="u-error u-margin-all-none u-margin-top-small">Please enter your name</p>
                      ))}
                    </div>
                    <div className="o-layout__item u-margin-bottom-small">
                      <label className="c-text-body" htmlFor="message">Message</label>
                      <textarea className="c-input"
                        name="message"
                        value={message}
                        onChange={this.handleChange}
                      />
                      {(submitted && !message && (
                        <p className="u-error u-margin-all-none u-margin-top-small">Please enter your message</p>
                      ))}
                    </div>
                    <div className="o-layout__item">
                      <div className="o-layout o-layout--right">
                        <div className="o-layout__item u-width-auto">
                          {name && message && (
                            <button className="c-btn">
                              Send
                            </button>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            )}
            <button className="c-btn c-btn--close" onClick={closeModal}>Close</button>
          </div>
        </div>
      </div>
    );
  }
}

Message.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  closeModal: PropTypes.func.isRequired
};

export default Message;
