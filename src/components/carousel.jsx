import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { viewWidth } from '../utils/dom';

class Carousel extends Component {
  constructor(props) {
    super(props);

    this.goBack = this.goBack.bind(this);
    this.goForward = this.goForward.bind(this);
    this.updateDimensions = this.updateDimensions.bind(this);

    this.state = {
      currentView: 0,
      viewPort: null
    };
  }

  componentDidMount() {
    this.updateDimensions();
    window.addEventListener('resize', this.updateDimensions);
  }

  updateDimensions() {
    const newWidth = viewWidth();
    if (newWidth > 740 && this.state.viewPort <= 740) {
      if (this.state.currentView > 1) {
        this.setState({ viewPort: newWidth, currentView: 1 });
      } else {
        this.setState({ viewPort: newWidth, currentView: 0 });
      }
    }
    this.setState({ viewPort: newWidth });
  }

  goForward() {
    this.setState({ currentView: this.state.currentView + 1 });
  }

  goBack() {
    if (this.state.currentView > 0) {
      this.setState({ currentView: this.state.currentView - 1 });
    }
  }

  render() {
    const { children } = this.props;
    const { currentView, viewPort } = this.state;
    return (
      <div className="o-carousel">
        {
          currentView > 0 && (
            <div tabIndex="0"
              role="button"
              className="o-carousel-back"
              onClick={this.goBack} />
          )
        }
        <div className="o-carousel-container">
          <div className="o-carousel-scroll"
            style={{ marginLeft: `-${currentView * 100}%` }}>
            { children }
          </div>
        </div>
        {
          (!viewPort || (viewPort > 740 && currentView < 1) || (viewPort <= 740 && currentView < 3)) &&
          <div tabIndex="0"
            role="button"
            className="o-carousel-next"
            onClick={this.goForward} />
        }
      </div>
    );
  }
}

Carousel.propTypes = { children: PropTypes.node.isRequired };

export default Carousel;
