import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';

import Page from './page';
import Home from './pages/home';
import About from './pages/about';
import NotFound from './nav/not-found';
import Shows from './pages/shows';
import Music from './pages/music';
import Watch from './pages/watch';

const App = ({ store }) => (
  <Provider store={store}>
    <Page>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={About} />
        <Route path="/shows" component={Shows} />
        <Route exact path="/music" component={Music} />
        <Route exact path="/watch" component={Watch} />
        <Route component={NotFound} />
      </Switch>
    </Page>
  </Provider>
);

App.propTypes = { store: PropTypes.object.isRequired };

export default App;
