import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { connect } from 'react-redux';
import moment from 'moment-timezone';
import _ from 'lodash';

const numberToString = i => i < 10 ? `0${i}` : i;

const timeToString = (date) => {
  const start = moment()
    .tz('Europe/London')
    .hours(0, 0, 0, 0);

  const end = moment()
    .tz('Europe/London')
    .hours(24, 0, 0, 0);

  let hour = date.hours();
  let minutes = date.minutes();

  if (date.isBefore(start)) {
    hour = 0;
    minutes = 0;
  }

  if (date.isAfter(end)) {
    hour = 24;
    minutes = 0;
  }
  return `${numberToString(hour)}:${numberToString(minutes)}`;
};

class Show extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      playing: 'paused',
      onNow: this.findOnNow()
    };
  }

  componentDidMount() {
    this.createAudioElement();
    this.startRefresher();
  }

  createAudioElement() {
    // Cache buster using current timestamp. Forces the browser to reload the livestream.
    const time = moment().unix();
    const streamUrl = `https://streams.warwickradio.org/raw-high.mp3?time=${time}`;

    this.audio = new Audio();
    this.audio.preload = 'none';
    this.audio.src = streamUrl;
  }

  startRefresher() {
    const endTime = this.state.onNow.end * 1000;

    setTimeout(() => {
      this.setState({ onNow: this.findOnNow() });
      this.startRefresher();
    }, endTime - Date.now());
  }

  findOnNow() {
    const now = moment()
      .tz('Europe/London')
      .unix();
    const { schedule } = this.props;
    return _.find(schedule, show => show.start <= now && show.end > now);
  }

  handleClick() {
    if (this.state.playing === 'playing') {
      // Force audio to stop playing and loading in the background
      // this means that the stream will continue from live when play
      // is clicked
      this.audio.pause();
      this.audio.src = '';
      this.audio.load();

      // Create new audio element to replace old one
      // ensures that the stream is reloaded
      this.createAudioElement();

      this.setState({ playing: 'paused' });
    }
    if (this.state.playing === 'paused') {
      this.setState({ playing: 'loading' });

      this.audio.play()
        .then(() => {
          this.setState({ playing: 'playing' });
        })
        .catch(() => {
          this.setState({ playing: 'paused' });
        });
    }
  }

  render() {
    const { playing, onNow } = this.state;
    const buttonClass = classnames('c-player-button-play', {
      'play-icon': playing === 'paused',
      'pause-icon': playing === 'playing',
      'loading-icon': playing === 'loading'
    });

    return (
      <div className="c-show">
        { onNow && (
          <div className="c-show-text">
            <div className="c-text-lead">
              <span className="c-show-on-air">
                On Air
              </span>
            </div>
            <div className="c-text-body">{onNow.name}</div>
            <div className="c-text-body u-text-bold">
              {timeToString(moment(onNow.start * 1000).tz('Europe/London'))} - {timeToString(moment(onNow.end * 1000).tz('Europe/London'))}
            </div>
          </div>
        )}
        <div tabIndex="0"
          role="button"
          className="c-player-button"
          onClick={this.handleClick}
        >
          <div className={buttonClass} />
          <div className="c-player-button-text u-text-bold">
            {{
              playing: 'Pause',
              paused: 'Listen',
              loading: 'Loading'
            }[this.state.playing]}
          </div>
        </div>
        {onNow && (
          <div className="c-show-image"
            style={{
              backgroundImage: `url('https://media2.radio.warwick.ac.uk/static/shows/${onNow.showid}.jpg')`
            }}
          />
        )}
      </div>
    );
  }
}

Show.propTypes = {
  schedule: PropTypes.arrayOf(PropTypes.shape({
    start: PropTypes.number,
    end: PropTypes.number,
    showid: PropTypes.number,
    name: PropTypes.string,
    description: PropTypes.string
  })).isRequired
};

const mapStateToProps = ({ schedule }) => ({ schedule });

export default connect(mapStateToProps)(Show);
