import React from 'react';

import Logo from '../../components/logo';

import Show from './show';

export default () => (
  <div className="c-player-background">
    <div className="c-player-container">
      <div className="c-player">
        <div className="c-logo">
          <Logo />
        </div>
        <Show />
      </div>
    </div>
  </div>
);
