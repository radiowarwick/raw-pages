import React from 'react';
import { Link } from 'react-router-dom';

import Status from '../utils/status-route';

const NotFound = () => (
  <div className="o-container">
    <div className="o-layout u-margin-top u-margin-bottom">
      <div className="o-layout__item">
        <h1 className="c-heading-alpha">404</h1>
        <h2 className="c-heading-bravo">The page you are looking for could not be found</h2>
        <h3 className="c-heading-charlie">Please make sure you entered the address correctly</h3>
        <p className="c-text-body"><Link to="/">Return to Home</Link></p>
      </div>
    </div>
  </div>
);

export default () => (
  <Status code={404}>
    <NotFound />
  </Status>
);
