import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { NavLink } from 'react-router-dom';

import Logo from '../components/logo';

const links = [
  {
    title: 'Home',
    link: '/'
  },
  {
    title: 'Schedule',
    link: '/shows'
  },
  {
    title: 'Music',
    link: '/music'
  },
  {
    title: 'About',
    link: '/about'
  }
];

class NavBar extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleMessage = this.handleMessage.bind(this);
    this.closeNav = this.closeNav.bind(this);

    this.state = { open: false };
  }

  handleClick() {
    this.setState({ open: !this.state.open });
    // TODO: rewrite classname
    document.body.classList.add('u-body-lock');
  }

  closeNav() {
    this.setState({ open: false });
    // TODO: rewrite classname
    document.body.classList.remove('u-body-lock');
  }

  handleMessage(e) {
    e.preventDefault();
    this.props.openModal();
    this.closeNav();
  }

  render() {
    const burgerClass = classnames('c-header', { 'is-open': this.state.open });
    return (
      <header className={burgerClass}>
        <div className="o-container">
          <div tabIndex="0"
            role="button"
            className="hamburger"
            id="hamburger-1"
            onClick={this.handleClick}
          >
            <span className="line" />
            <span className="line" />
            <span className="line" />
          </div>
          <div className="c-header-logo">
            <Logo />
          </div>
          <nav className="c-nav-primary">
            <ul className="c-nav-links">
              {links.map(({ title, link }) =>
                <li key={title} className="c-nav-link">
                  <NavLink to={link}
                    onClick={this.closeNav}
                    exact={link === '/'}
                    activeStyle={{
                      fontWeight: 'bold'
                    }}
                  >
                    {title}
                  </NavLink>
                </li>
              )}
            </ul>
            <hr className="c-divider" />
            <div className="c-nav-secondary">
              <div className="c-nav-links">
                <li className="c-nav-link">
                  <a href="#" onClick={this.handleMessage}>
                    Message
                  </a>
                </li>
                <li className="c-nav-link">
                  <NavLink to="/watch"
                    onClick={this.closeNav}
                    activeStyle={{
                      fontWeight: 'bold'
                    }}
                  >
                    Watch
                  </NavLink>
                </li>
                <li className="c-nav-link">
                  <a href="https://space.radio.warwick.ac.uk/space/login">Sign in</a>
                </li>
              </div>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}

NavBar.propTypes = { openModal: PropTypes.func.isRequired };

export default NavBar;
