import React, { Component } from 'react';
import classnames from 'classnames';

const icons = [
  { name: 'twitter', link: 'https://twitter.com/raw1251am' },
  { name: 'facebook', link: 'https://facebook.com/raw1251am' },
  { name: 'instagram', link: 'https://instagram.com/raw1251am' },
  { name: 'youtube', link: 'https://youtube.com/raw1251am' }
];

export default class Social extends Component {
  constructor(props) {
    super(props);
    this.handleFocus = this.handleFocus.bind(this);
    this.state = { focused: null };
  }
  componentDidMount() {
    icons.forEach(({ name }) => {
      this.handleFocus(this[name], name);
    });
  }

  handleFocus(e, nodeName) {
    e.addEventListener('focus', () => {
      this.setState({ focused: nodeName });
    }, true);
    e.addEventListener('blur', () => {
      this.setState({ focused: null });
    }, true);
  }

  render() {
    return (
      <div className="c-social">
        <div>Follow Us:</div>
        { icons.map(({ name, link }) =>
          <div key={name}
            className={classnames(
              'social_icon',
              `${name}_icon`,
              { 'is-focused': this.state.focused === name }
            )}
          >
            <a ref={(node) => { this[name] = node; }} href={link}>{name}</a>
          </div>
        ) }
      </div>
    );
  }
}
