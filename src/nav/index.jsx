import React from 'react';
import PropTypes from 'prop-types';

import Nav from './nav';
import Footer from './footer';
import Player from './radioplayer';

const AppWrapper = ({ children, openModal }) => (
  <div className="application">
    <Player />
    <Nav openModal={openModal} />
    <main className="content">
      { children }
    </main>
    <Footer />
  </div>
);

AppWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  openModal: PropTypes.func.isRequired
};

export default AppWrapper;
