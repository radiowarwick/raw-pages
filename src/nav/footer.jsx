import React from 'react';

import Logo from '../components/logo';

import Follow from './follow';

export default () => (
  <footer className="c-footer">
    <div className="o-container u-padding-top-tiny u-padding-bottom-tiny">
      <div className="o-layout o-layout--middle">
        <div className="o-layout__item u-width-auto">
          <div className="o-layout o-layout--center">
            <div className="o-layout__item u-width-auto">
              <div className="c-footer__logo">
                <Logo />
              </div>
            </div>
            <div className="o-layout__item u-width-auto c-footer__smallprint">
              <p className="c-text-smallprint u-margin-bottom-none">&copy; Radio Warwick {new Date().getFullYear()}</p>
            </div>
          </div>
        </div>
      </div>
      <Follow />
    </div>
  </footer>
);
